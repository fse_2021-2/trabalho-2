#include "gpio_control.hpp"
#include "server.hpp"

#include <sys/socket.h>
#include <wiringPi.h>
#include <unistd.h>
#include <iostream>

#define MAX_MESSAGE_LENGTH 10000

using namespace std;

void* handle_inputs(void* parameters) {
    Server *server = (Server *) parameters;
    
    wiringPiSetupGpio();
    for(Item input : server->inputs) pinMode(input.gpio, INPUT);

    while(1) {
        system("clear");
        cout << "===== Sensores:\n\n";
        for(Item& input: server->inputs) {
            input.state = digitalRead(input.gpio);
            if(input.type != "contagem") {
                cout << input.tag << ": " << (input.state == 1 ? "Ligado\n" : "Desligado\n");
            }
        }
        cout << endl << endl;
        usleep(500000);
    }
}


// Segmentation Fault
void* handle_outputs(void* parameters) {
    Server *server = (Server *) parameters;
    char received_message[MAX_MESSAGE_LENGTH];

    wiringPiSetupGpio();
    for(Item output : server->outputs) {
        pinMode(output.gpio, OUTPUT);
        digitalWrite(output.gpio, LOW); // Desliga todas as cargas
    }

    // Verifica por mensagens do servidor central a cada 1s
    while(1) {
        memset(received_message, 0, MAX_MESSAGE_LENGTH);
        int message_length;
        
        if ((message_length = recv(server->socket, received_message, MAX_MESSAGE_LENGTH, 0)) > 0) {
            json message = json::parse(received_message);
            digitalWrite(message["pin"], message["value"]);
        }
        
        sleep(1);
    }
}