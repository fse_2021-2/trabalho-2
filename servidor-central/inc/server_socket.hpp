#ifndef SERVER_SOCKET_HPP
#define SERVER_SOCKET_HPP

#include <sys/socket.h>
#include <arpa/inet.h>

int socket_init();

#endif