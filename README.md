# FSE: Trabalho 2 - 2021.2.

**Nome:** Hérick Ferreira de Souza Portugues

**Matrícula:** 18/0033034

**Descrição:** Este trabalho refere-se ao definido em: https://gitlab.com/fse_fga/trabalhos-2021_2/trabalho-2-2021-2

## Observações
 - O projeto possui requisitos não finalizados/implementados.

## Servidor Central

O servidor central inicializa o socket do servidor e aguarda por servidores clientes. Ao estabeler conexão, ele recebe os dados do cliente e possiblita ao usuário controlar os dados de de saída - o controle não está funcional, já que o servidor distribuído não está conseguindo receber a mensagem e acionar a carga. O monitoramento dos dados de input, contagem de pessoas, controle de alarmes, etc, não foram implementados. 

Rodando o servidor central:
```
cd servidor-central
make
make run
```

## Servidores Distribuídos

Os servidores distribuídos são inicializados pelo arquivo de configuração json e estabelecem conexão com o servidor central. Além disso, há uma thread obtendo os valores de input atualizados por meio da técnica de polling. A contagem de pessoas, verificação de temperatura/umidade, a thread que recebe as mensagens do servidor central e aciona as cargas, etc não foram finalizadas.

Rodando os servidores distribuidos:
```
cd servidor-distribuido
make
make run file=caminho_do_arquivo
```
