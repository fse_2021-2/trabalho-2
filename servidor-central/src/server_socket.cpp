#include "server_socket.hpp"
#include "json.hpp"

#include <iostream>

#define SERVER_PORT 10039

using namespace std;

static struct sockaddr_in server_addr;

int socket_init() {
    int server_socket;
    
    if((server_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        cout << "Falha no socket do servidor central\n";
        exit(1);
    }

    memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(SERVER_PORT);
    
    if(bind(server_socket, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
        cout << "Falha no Bind\n";
        exit(1);
    }

    if(listen(server_socket, 2) < 0) {
        cout << "Falha no Listen\n";
        exit(1);
    }

    return server_socket;
}