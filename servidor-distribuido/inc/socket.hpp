#ifndef SOCKET_HPP
#define SOCKET_HPP

#include "server.hpp"

int socket_init(Server server);
void socket_connect(int);
void send_client_server(int, Server);
void socket_close();

#endif