#include "server_socket.hpp"
#include "json.hpp"

#include <iostream>
#include <pthread.h>
#include <vector>
#include <string>
#include <unistd.h>

#define MAX_MESSAGE_LENGTH 100000

using json = nlohmann::json;
using namespace std;

vector<json> clients;

void* handle_connections(void* arg) {
    int* server_socket = (int *) arg;

    int client_socket;
    unsigned int client_length;
    struct sockaddr_in client_addr;
    char client_server[MAX_MESSAGE_LENGTH];
    int received_message_length;

    // Tenta estabelecer conexão com um cliente a cada 1s
    while(true) {
        client_length = sizeof(client_addr);
        if((client_socket = accept(*server_socket, (struct sockaddr *) &client_addr, &client_length)) >= 0) {
            memset(&client_server, 0, MAX_MESSAGE_LENGTH);
            do {
                received_message_length = recv(client_socket, client_server, MAX_MESSAGE_LENGTH, 0);
            } while(received_message_length < 0);
            json j = json::parse(client_server);
            j["client_socket"] = client_socket;
            clients.push_back(j);
        }
        sleep(1);
    }
}

int main() {
    pthread_t sockets_thread;
    int server_socket = socket_init();

    // Thread que lida com as conexões do clientes
    pthread_create(&sockets_thread, NULL, handle_connections, &server_socket);

    int command;
    do {
        system("clear");
        int count = 0;

        cout << "========== MENU DE CONTROLE ==========\n\n";
        if (clients.size() == 0) {
            cout << "Nenhum servidor conectado!!!\n";
            sleep(1);
        } else {
            cout << "Selecione o número indicado para inverter o estado\n\n";
            for(auto client: clients) {
                cout << "Servidor: " << client["name"] << endl;
                cout << "Saídas:\n";
                for(auto output : client["outputs"]) {
                    cout << "\t" << count++ << " -> " << output["tag"] << ": " << (output["state"] == 1 ? "Ligado\n" : "Desligado\n");
                }
                cout << endl;
            }
            cout << "\nComando: ";
            cin >> command;

            // Envia mensagem para o cliente
            int first_client_outputs_size = clients[0].size(), i = 1;
            if(command >= 0 and command <= count) {
                if (command < first_client_outputs_size) {
                    i = 0;
                    first_client_outputs_size = 0;
                }
                
                json message;
                message["pin"] = clients[i]["outputs"][command - first_client_outputs_size]["gpio"];
                if(clients[i]["outputs"][command - first_client_outputs_size]["state"] == 0) {
                    clients[i]["outputs"][command - first_client_outputs_size]["state"] = 1;
                } else {
                    clients[i]["outputs"][command - first_client_outputs_size]["state"] = 0;
                }
                message["value"] = clients[i]["outputs"][command - first_client_outputs_size]["state"];
                send(clients[i]["client_socket"], message.dump().c_str(), message.dump().size(), 0);
                cout << message.dump(4) << endl;
            }
        }
    } while(1);

    return 0;
}