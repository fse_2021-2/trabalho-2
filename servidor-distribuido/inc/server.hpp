#ifndef SERVER_HPP
#define SERVER_HPP

#include "json.hpp"

#include <string>
#include <vector>

using json = nlohmann::json;
using namespace std;

struct Item {
    string type;
    string tag;
    int gpio;
    volatile int state;
};

struct Server {
    string name;
    string ip;
    int port;
    string centraL_server_ip;
    int central_server_port;
    vector<Item> outputs;
    vector<Item> inputs;
    vector<Item> temperature_sensor;
    int socket;
};

void to_json(json&, const Item&);
void from_json(const json&, Item&);
void to_json(json&, const Server&);
void from_json(const json&, Server&);

#endif