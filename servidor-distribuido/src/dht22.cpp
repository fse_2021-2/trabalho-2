#include "dht22.hpp"

#include <iostream>

using namespace std;

float temperature = -1;
float humidity  = -1;

int dht22_read(uint8_t dht_pin) {
	wiringPiSetupGpio();

	uint8_t laststate = HIGH;
	uint8_t counter	= 0;
	uint8_t j = 0;
	uint8_t i;

	int data[5] = { 0, 0, 0, 0, 0 };

	/* pull pin down for 18 milliseconds */
	pinMode(dht_pin, OUTPUT);
	digitalWrite(dht_pin, LOW);
	delay(18);

	/* prepare to read the pin */
	pinMode(dht_pin, INPUT);

	/* detect change and read data */
	for ( i = 0; i < MAX_TIMINGS; i++ ) {
		counter = 0;
		while(digitalRead(dht_pin) == laststate) {
			counter++;
			delayMicroseconds(1);
			if (counter == 255) {
				break;
			}
		}
		laststate = digitalRead( dht_pin );

		if (counter == 255)
			break;

		/* ignore first 3 transitions */
		if((i >= 4) && (i % 2 == 0)) {
			/* shove each bit into the storage bytes */
			data[j / 8] <<= 1;
			if (counter > 16)
				data[j / 8] |= 1;
			j++;
		}
	}

	/*
	 * check we read 40 bits (8bit x 5 ) + verify checksum in the last byte
	 * print it out if data is good
	 */
	if ((j >= 40) && (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF))) {
		float h = (float)((data[0] << 8) + data[1]) / 10;
		float c = (float)(((data[2] & 0x7F) << 8) + data[3]) / 10;
		if (data[2] & 0x80) {
			c = -c;
		}
		temperature = c;
		humidity = h;
        cout << "Umidade: " << humidity << "%\n";
        cout << "Temperatura: " << temperature << "º\n";
		return 0; // OK
	} else {
		temperature = humidity = -1;
		return 1; // NOK
	}
}