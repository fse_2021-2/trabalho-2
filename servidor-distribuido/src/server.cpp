#include "server.hpp"

void to_json(json& j, const Item& item) {
    j = json{{"type", item.type}, {"tag", item.tag}, {"gpio", item.gpio}, 
        {"state", item.state}};
}

void from_json(const json& j, Item& item) {
    j.at("type").get_to(item.type);
    j.at("tag").get_to(item.tag);
    j.at("gpio").get_to(item.gpio);
}

void to_json(json& j, const Server& server) {
    j = json{{"name", server.name}, {"outputs", server.outputs}, 
        {"inputs", server.inputs}};
}

void from_json(const json& j, Server& server) {
    j.at("nome").get_to(server.name);
    j.at("ip_servidor_distribuido").get_to(server.ip);
    j.at("porta_servidor_distribuido").get_to(server.port);
    j.at("ip_servidor_central").get_to(server.centraL_server_ip);
    j.at("porta_servidor_central").get_to(server.central_server_port);
    j.at("outputs").get_to(server.outputs);
    j.at("inputs").get_to(server.inputs);
    j.at("sensor_temperatura").get_to(server.temperature_sensor);
}
