#include "gpio_control.hpp"
#include "socket.hpp"
#include "server.hpp"
#include "dht22.hpp"

#include <iostream>
#include <fstream>
#include <thread>
#include <pthread.h>
#include <unistd.h>

using namespace std;

int main(int argc, char** argv) {
    if(argc == 1) {
      cout << "É necessário passar um arquivo de entrada\n";
      exit(1);
    }

    // json file to Server
    ifstream json_file(argv[1]);
    json data;
    json_file >> data;
    auto server = data.get<Server>();

    // Configura socket e conecta com o servidor central
    int client_socket = socket_init(server);
    server.socket = client_socket;
    socket_connect(client_socket);
    send_client_server(client_socket, server);

    sleep(2);
    system("clear");
    cout << "===== Servidor Distribuído do " << server.name << " =====\n";

    pthread_t thread_inputs, thread_outputs;
    pthread_create(&thread_inputs, NULL, &handle_inputs, &server);
    pthread_join(thread_inputs, NULL);

    // while(1) {
    //   dht22_read(server.temperature_sensor[0].gpio);
    //   sleep(1);
    // }

    // pthread_create(&thread_outputs, NULL, &handle_outputs, &server);

    return 0;
}
