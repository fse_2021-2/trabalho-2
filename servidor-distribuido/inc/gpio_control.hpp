#ifndef GPIO_CONTROL_HPP
#define GPIO_CONTROL_HPP

/* Thread responsável pela leitura dos sensores de input a cada 500ms */
void* handle_inputs(void*);

/* Thread responsável pela leitura de mensagens do servidor central 
    e envio dos sinais dos pinos de saída */
void* handle_outputs(void*);

#endif