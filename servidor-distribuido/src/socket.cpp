#include "socket.hpp"
#include "json.hpp"

#include <iostream>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

using namespace std;

static struct sockaddr_in server_addr;

int socket_init(Server server) {
    int client_socket;

	if((client_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        cout << "Erro no socket()\n";
        exit(1);
    }

	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(server.centraL_server_ip.c_str());
	server_addr.sin_port = htons(server.central_server_port);

    return client_socket;
}

void socket_connect(int client_socket) {
    while(1) {
        cout << "Aguardando conexão com o servidor central ...\n";
        if(connect(client_socket, (struct sockaddr *) &server_addr, sizeof(server_addr)) >= 0) {
            cout << "\nConexão Estabelecida\n";
            break;
        }

        sleep(1);
    }
}

void send_client_server(int client_socket, Server server) {
    json j = server;
    send(client_socket, j.dump().c_str(), j.dump().length(), 0);
}

// void socket_close() {
//     close(client_socket);
// }